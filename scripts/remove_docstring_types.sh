#!/bin/bash

function remove_docstring_types {
    # Remove most standard docstring type information
    sed -r -i \
        -e '/[ ]+.*"""$/s/:type [^:]+: [^"]+//g' \
        -e '/ :type [^:]+: .*/d' \
        -e '/[ ]+.*"""$/s/:rtype: [^"]+//g' \
        -e '/ :rtype: .+/d' \
        $1
}

DOCTYPED_MODULES=$(rgrep -Icl "    :r\?type" src/sardana)

for MODULE in ${DOCTYPED_MODULES[@]}
do
    echo $MODULE
    remove_docstring_types $MODULE
done
